# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2 or later

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_USE_PEP517=setuptools

inherit distutils-r1

DESCRIPTION="A Python wrapper which confines processes to processor and memory node subsets"
HOMEPAGE="https://github.com/SUSE/cpuset"
SRC_URI="
		https://github.com/SUSE/cpuset/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64"
# Tests must be conducted manually, there are no automatic ones
RESTRICT="test"

python_prepare_all() {
	# Comply with FHS/Gentoo policies
	sed -i -e "s|share/doc/packages/cpuset|share/doc/${PF}|" setup.py

	distutils-r1_python_prepare_all
}

python_install_all() {
	doman "${S}"/doc/*.1
	distutils-r1_python_install_all
}
