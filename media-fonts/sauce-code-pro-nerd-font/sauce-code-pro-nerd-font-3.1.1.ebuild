# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2 or later

EAPI=8

S="${WORKDIR}"
FONT_SUFFIX="ttf"
inherit font

DESCRIPTION="Adobe Source Code Pro font patched with Nerd Font glyphs"
HOMEPAGE="https://github.com/ryanoasis/nerd-fonts"
SRC_URI="https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/SourceCodePro.tar.xz -> ${PN}-${PV}.tar.xz"

LICENSE="OFL-1.1"
SLOT="0"
KEYWORDS="~amd64"

DOCS=( README.md LICENSE.txt )
