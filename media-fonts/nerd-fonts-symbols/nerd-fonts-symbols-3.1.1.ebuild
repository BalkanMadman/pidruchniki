# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2 or later

EAPI=8

S="${WORKDIR}"
FONT_SUFFIX="ttf"
inherit font

DESCRIPTION="A collection of symbols from Nerd Fonts"
HOMEPAGE="https://github.com/ryanoasis/nerd-fonts"
SRC_URI="https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/NerdFontsSymbolsOnly.tar.xz -> ${PN}-${PV}.tar.xz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DOCS=( README.md LICENSE )
